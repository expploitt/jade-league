#JADE GAME#

     Author: Moisés Carral Ortiz (@expploitt)

### How to ###

 * Set enviroment: unzip the .zip file with unzip MCO_GAME.zip MCO_GAME/
   
 * Compile and run: there is an bash script to compile and run the game. You will must 
modify the line of the script that launch the agents. Add the name and types of the 
differents agent in order to create your ideal league.


### Agents###

 * Intelligent_2: this agent implements a Q-learning algorithm. It creates as many 
states as columns/row have the matrix. For example if you have a 3x3 matrix, it will  
have 3 states with 3 possible actions: to choose the same column/row (states) or 
choose 
other posible column/row (state). If the subtraction of the scores of that round 
was positive for the player (my score was better than my opponent's) the agent will 
learn, otherwise it will not.
    
 * Intelligent_3: this agent implements a Q-learning algorithm. It have one 
state that consists of S actions. It will learn in case of better score than my 
opponent's or equal. Actually, the improvement over the intelligent_2 agent is the 
convergence's time, it's smaller.

 * Intelligent_4: this agent implements a Q-learning algorithm. It have only one state 
with S possibles options. Now, the regard of each action is the difference of 
the average score of the agent's window point buffer and the oponent's. Furthermore, 
there is a buffer to know what where the last positions of the opponent to decide if 
the opponent is like a fixedPlayer. In that case, the agent (how is tracking the 
matrix), will choose the best option for the fixed column/row chosen by the opponent. 
It will try to get the maximum score. Otherwise it will try a tie or to minimize the 
opponent's score.




