#!/bin/bash

if [ ! -d "bin" ]; then
  mkdir bin
fi

#Command to compile all the *.java files
javac -classpath lib/jade.jar:lib/absolutelayout/AbsoluteLayout.jar: -d bin/ src/PSI5/*.java

#Command to run the game
java -cp lib/jade.jar:lib/absolutelayout/AbsoluteLayout.jar:bin/:. jade.Boot  -agents "F1:PSI5.FixedPlayer;M:PSI5.MainAgent;I4:PSI5.Intelligent_4;I3:PSI5.Intelligent_3;I2:PSI5.Intelligent_2;R:PSI5.RandomPlayer" 


##################
#TIPES OF PLAYERS#
##################

#name:PSI5.FixedPlayer
#name:PSI5.RandomPlayer
#name:PSI5.Intelligent_2
#name:PSI5:Intelligent_3
#name:PSI5:Intelligent_4
