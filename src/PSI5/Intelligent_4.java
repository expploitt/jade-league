/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author expploitt
 */
public class Intelligent_4 extends Agent {

    final double dDecFactorLR = 0.99;			// Value that will decrement the learning rate in each generation
    final double dEpsilon = 0.2;				// Used to avoid selecting always the best action
    final double dMINLearnRate = 0.05;			// We keep learning, after convergence, during 5% of times
    final double dGamma = 0.6;

    double dLastFunEval;
    double dLearnRate = 0.9;

    int iNumActions = 0; 					// For C or D for instance
    int iLastAction;					// The last action that has been played by this player
    int iAction;
    int iNewAction;

    int[] iNumTimesAction;		// Number of times an action has been played
    int[][] dPayoffAction;	// Accumulated payoff obtained by the different actions
    int[][] dPayoffActionOp;
    int[] iActionsOponent;
    Componente[][] matrix;

    StateAction oPresentStateAction;			// Contains the present state we are and the actions that are available
    Vector oVStateActions;					// A vector containing strings with the possible States and Actions available at each one
    StateAction oLastStateAction;

    private int id, R, I, S, N, P, G;
    private int position = 0;

    protected void setup() {

        System.out.println("Hola, soy un Intelligent Agent de tipo 4 y me llamo " + getAID().getName());

        //Register in the yellow pages as a player
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Player");
        sd.setName("Game");
        dfd.addServices(sd);

        oVStateActions = new Vector();

        try {
            DFService.register(this, dfd);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }

        addBehaviour(new Intelligent_4.GamePerformer());
    }

    @Override
    public void takeDown() {
        System.out.println("Intelligent Agent de tipo 1 terminando...");
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is used to implement Q-Learning: 1. I start with the last
     * action a, the previous state s and find the actual state s' 2. Select the
     * new action with Qmax{a'} 3. Adjust: Q(s,a) = Q(s,a) + dLearnRateLR [R +
     * dGamma . Qmax{a'}(s',a') - Q(s,a)] 4. Select the new action by a
     * epsilon-greedy methodology
     *
     * @param sState contains the present state
     * @param iNActions contains the number of actions that can be applied in
     * this state
     * @param dFunEval is the new value of the function to evaluate (depends on
     * the game). This value minus last value also determines the reward to be
     * used.
     */
    public void vGetNewActionQLearning(String sState, int iNActions, double dFunEval) {
        boolean bFound;
        int iBest = -1, iNumBest = 1;
        double dQmax;
        StateAction oStateAction;

        bFound = false;							// Searching if we already have the state
        for (int i = 0; i < oVStateActions.size(); i++) {
            oStateAction = (StateAction) oVStateActions.elementAt(i);
            if (oStateAction.sState.equals(sState)) {
                oPresentStateAction = oStateAction;
                bFound = true;
                break;
            }
        }
        // If we didn't find it, then we add it
        if (!bFound) {
            oPresentStateAction = new StateAction(sState, iNActions);
            oVStateActions.add(oPresentStateAction);
        }

        dQmax = 0;
        for (int i = 0; i < iNActions; i++) {					// Determining the action to get Qmax{a'}
            if (oPresentStateAction.dValAction[i] > dQmax) {
                iBest = i;
                iNumBest = 1;							// Reseting the number of best actions
                dQmax = oPresentStateAction.dValAction[i];
            } else if ((oPresentStateAction.dValAction[i] == dQmax) && (dQmax > 0)) {	// If there is another one equal we must select one of them randomly
                iNumBest++;
                if (Math.random() < 1.0 / (double) iNumBest) {				// Choose randomly with reducing probabilities
                    iBest = i;
                    dQmax = oPresentStateAction.dValAction[i];
                }
            }
        }
        // Adjusting Q(s,a)
        if (oLastStateAction != null) {
            if (dFunEval > 0) { // If reward grows and the previous action was allowed --> reinforce the previous action considering present values
                if (dFunEval > 0) { //I decrement the values to get lower Q values
                    dFunEval = dFunEval / 10;
                }

                System.out.println("dValAction = " + oLastStateAction.dValAction[iAction]);
                oLastStateAction.dValAction[iAction] += dLearnRate * (dFunEval + dGamma * dQmax - oLastStateAction.dValAction[iAction]);
                System.out.println("dValAction = " + oLastStateAction.dValAction[iAction]);
            }
        }

        if ((iBest > -1) && (Math.random() > dEpsilon)) // Using the e-greedy policy to select the best action or any of the rest
        {
            iNewAction = iBest;
            System.out.println("Best answer " + iNewAction);
        } else {
            do {
                iNewAction = (int) (Math.random() * (double) iNumActions);
                System.out.println("Random Answer " + iNewAction);
            } while (iNewAction == iBest);
        }

        oLastStateAction = oPresentStateAction;				// Updating values for the next time
        dLastFunEval = dFunEval;
        dLearnRate *= dDecFactorLR;						// Reducing the learning rate
        if (dLearnRate < dMINLearnRate) {
            dLearnRate = dMINLearnRate;
        }
    }

    private class StateAction implements Serializable {

        String sState;
        double[] dValAction;

        StateAction(String sAuxState, int iNActions) {
            sState = sAuxState;
            dValAction = new double[iNActions];
        }

        public String sGetState() {
            return sState;
        }

        public double dGetQAction(int i) {
            return dValAction[i];
        }
    }

    /**
     *
     */
    private class GamePerformer extends CyclicBehaviour {

        private int step = 0;
        private ACLMessage msg, reply;
        private MessageTemplate mt;
        private int tR = R, tI = I;
        private boolean playerA;

        @Override
        public void action() {
            String[] aux;
            switch (step) {
                case 0: //get ID of player
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo ID");
                        aux = msg.getContent().split("#");
                        id = Integer.parseInt(aux[1]);
                        System.out.println(getAID() + " " + id);
                        aux = aux[2].split(",");

                        N = Integer.parseInt(aux[0]);
                        S = Integer.parseInt(aux[1]);
                        tR = R = Integer.parseInt(aux[2]);
                        tI = I = Integer.parseInt(aux[3]);
                        P = Integer.parseInt(aux[4]);
                        G = N - 1;

                        iNumActions = S;

                        step++;
                    } else {
                        block();
                    }

                    break;
                case 1:  //get message of NEW GAME
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "Recibido mensaje " + aux[0]);
                        aux = aux[1].split(",");
                        System.out.println(getAID() + "Vamos a jugar jugador " + aux[0] + " y jugador " + aux[1]);

                        if (id == Integer.parseInt(aux[0])) {
                            playerA = true;
                            System.out.println("Is player A");
                        }

                        oVStateActions.add(0, new StateAction("state0", S));
                        oPresentStateAction = (StateAction) oVStateActions.get(0);
                        oLastStateAction = (StateAction) oVStateActions.get(0);
                        dPayoffAction = new int[S][3];
                        dPayoffActionOp = new int[S][3];
                        iNumTimesAction = new int[S];
                        iActionsOponent = new int[4];
                        iActionsOponent[0] = 30;
                        iActionsOponent[1] = 30;
                        iActionsOponent[2] = 30;
                        iActionsOponent[3] = 30;
                        matrix = new Componente[S][S];
                        dLearnRate = 0.9;

                        step++;
                    } else {
                        block();
                    }

                    break;
                case 2:  //REQUEST for choose position 
                    mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo REQUEST de Position");
                        reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setConversationId("Position");

                        position = iNewAction;
                        reply.setContent("Position#" + position);
                        System.out.println("Position = " + position);

                        send(reply);
                        tR--;
                        tI--;
                        step++;
                    } else {
                        block();
                    }
                    break;
                case 3:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo PUNTOS");

                        int difPoints, myValue, opntValue, positionOp;

                        if (playerA) {
                            positionOp = Integer.parseInt(msg.getContent().split("#")[1].split(",")[1]);
                            myValue = Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]);
                            opntValue = Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]);
                            difPoints = myValue - opntValue;
                            matrix[position][positionOp] = new Componente(myValue, opntValue);
                            matrix[positionOp][position] = new Componente(myValue, opntValue);
                        } else {
                            positionOp = Integer.parseInt(msg.getContent().split("#")[1].split(",")[0]);
                            myValue = Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]);
                            opntValue = Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]);
                            difPoints = myValue - opntValue;
                            matrix[positionOp][position] = new Componente(opntValue, myValue);
                            matrix[position][positionOp] = new Componente(opntValue, myValue);
                        }

                        /*Desplazamos los bufferes tanto de puntuaciones anteriores como de las posiciones elegidas por el oponente*/
                        for (int i = 0; i < dPayoffAction[position].length - 1; i++) {
                            dPayoffAction[position][i] = dPayoffAction[position][i + 1];
                        }

                        for (int i = 0; i < dPayoffActionOp[positionOp].length - 1; i++) {
                            dPayoffActionOp[positionOp][i] = dPayoffActionOp[positionOp][i + 1];
                        }

                        for (int i = 0; i < iActionsOponent.length - 1; i++) {
                            iActionsOponent[i] = iActionsOponent[i + 1];
                        }


                        /*Add the new values obtained in each buffer*/
                        dPayoffAction[position][dPayoffAction[position].length - 1] = myValue;
                        iNumTimesAction[position] += 1;

                        dPayoffActionOp[positionOp][dPayoffActionOp[positionOp].length - 1] = opntValue;

                        iActionsOponent[iActionsOponent.length - 1] = positionOp;

                        /*Code only for debug
                         *
                        System.out.println("-- My Buffers --");
                        printPayoff(dPayoffAction);
                        System.out.println("-- Buffers Oponent --");
                        printPayoff(dPayoffActionOp);

                        System.out.print("MovOponente: [ ");
                        for (int i = 0; i < iActionsOponent.length; i++) {
                            System.out.print(iActionsOponent[i]);
                        }
                        System.out.println(" ]");
                         */
                        boolean fixed = false;

                        for (int i = 1; i < iActionsOponent.length; i++) {
                            if (iActionsOponent[0] != iActionsOponent[i]) {
                                fixed = false;
                                break;
                            } else {
                                fixed = true;
                            }
                        }

                        int maxPosition = 0, posibleMax = 0, posibleMin = 0, minPosition = 10, tiePosition = 0;
                        boolean isMax = false, isTie = false, isMin = false;

                        if (!fixed) {
                            /*The strategy of the oponent is not like a fixed player*/
                            System.out.println("-------------- NO FIJO ------------------");
                            /*Calculate the avgs of points for each row/col*/
                            double media = 0, mediaOp = 0;

                            for (int i = 0; i < dPayoffAction[position].length; i++) {
                                media += dPayoffAction[position][i];
                            }

                            for (int i = 0; i < dPayoffActionOp[positionOp].length; i++) {
                                mediaOp += dPayoffActionOp[positionOp][i];
                            }

                            System.out.println("media: " + (media / dPayoffAction[position].length));
                            System.out.println("mediaOp: " + (mediaOp / dPayoffActionOp[positionOp].length));

                            vGetNewActionQLearning(oPresentStateAction.sGetState(), S, media - mediaOp);
                            iAction = iNewAction;
                        } else {
                            /*The strategy of the oponent is always to choose the same row/col */
                            System.out.println("-------------- FIJO ------------------");
                            if (playerA) { //is player A
                                for (int i = 0; i < matrix.length; i++) {
                                    /*Is there any position in the matrix that I haven't known yet?*/
                                    if (matrix[i][iActionsOponent[0]] != null) {
                                        if (matrix[i][iActionsOponent[0]].getI() >= matrix[i][iActionsOponent[0]].getJ()) {
                                            posibleMax = matrix[i][iActionsOponent[0]].getI();
                                            if ((posibleMax - matrix[i][iActionsOponent[0]].getJ()) > 0) {
                                                /*There is a max index*/
                                                maxPosition = i;
                                                System.out.println("Hay nuevo maximo");
                                                isMax = true;
                                            } else if ((posibleMax - matrix[i][iActionsOponent[0]].getJ()) == 0 && !isMax && matrix[i][iActionsOponent[0]].getI() > matrix[tiePosition][iActionsOponent[0]].getI()) {
                                                /*There is a tie*/
                                                tiePosition = i;
                                                System.out.println("Hay nuevo empate");
                                                isTie = true;
                                            }
                                        } else {
                                            if (matrix[i][iActionsOponent[0]] != null && matrix[i][iActionsOponent[0]].getJ() < posibleMin) {
                                                /*I can't get a tie or a max index so I try to minimize the oponent poinst*/
                                                posibleMin = matrix[i][iActionsOponent[0]].getJ();
                                                isMin = true;
                                                System.out.println("Hay nuevo minimo");
                                                minPosition = i;
                                            }
                                        }
                                    } else {
                                        /*If we don't know all the index I'm goint to try to discover it*/
                                        maxPosition = i;
                                        isMax = isMin = isTie = false;
                                        System.out.println("Salimos, hay posición nula");
                                        break;
                                    }
                                }

                                if (isMax) { //there is a max position
                                    iNewAction = maxPosition;
                                    System.out.println("Maxposition: " + maxPosition);
                                } else if (isTie) { //there is a tie position
                                    iNewAction = tiePosition;
                                    System.out.println("Maxposition_empate: " + maxPosition);
                                } else if (isMin) { //there is a min position
                                    iNewAction = minPosition;
                                    System.out.println("Minposition: " + minPosition);
                                } else { //There is a position to be discovered
                                    iNewAction = maxPosition;
                                    System.out.println(maxPosition);
                                }

                            } else { //not playerA
                                /*It's the same code of player A but with the other perspective of the matrix*/
                                for (int i = 0; i < matrix.length; i++) {
                                    if (matrix[iActionsOponent[0]][i] != null) {
                                        if (matrix[iActionsOponent[0]][i].getJ() >= matrix[iActionsOponent[0]][i].getI()) {
                                            posibleMax = matrix[iActionsOponent[0]][i].getJ();
                                            if ((posibleMax - matrix[iActionsOponent[0]][i].getI()) > 0) {
                                                maxPosition = i;
                                                System.out.println("Hay nuevo maximo");
                                                isMax = true;
                                            } else if ((posibleMax - matrix[iActionsOponent[0]][i].getI()) == 0 && !isMax && matrix[iActionsOponent[0]][i].getJ() > matrix[iActionsOponent[0]][tiePosition].getJ()) {
                                                tiePosition = i;
                                                System.out.println("Hay nuevo empate");
                                                isTie = true;
                                            }
                                        } else {
                                            if (matrix[iActionsOponent[0]][i] != null && matrix[iActionsOponent[0]][i].getI() < posibleMin) { //minimizo el valor del contrario
                                                posibleMin = matrix[iActionsOponent[0]][i].getI();
                                                isMin = true;
                                                System.out.println("Hay nuevo minimo");
                                                minPosition = i;
                                            }
                                        }
                                    } else {
                                        maxPosition = i;
                                        isMax = isMin = isTie = false;
                                        System.out.println("Salimos, hay posición nula");
                                        break;
                                    }
                                }

                                if (isMax) { //hay maximo
                                    iNewAction = maxPosition;
                                    System.out.println("Maxposition: " + maxPosition);
                                } else if (isTie) { //hay empate
                                    iNewAction = tiePosition;
                                    System.out.println("Maxposition_empate: " + maxPosition);
                                } else if (isMin) { //hay mínimo como última opción
                                    iNewAction = minPosition;
                                    System.out.println("Minposition: " + minPosition);
                                } else {
                                    iNewAction = maxPosition;
                                    System.out.println(maxPosition);
                                }

                            }
                        }

                        if (tR == 0) {
                            step = 5;
                        } else if (tI == 0) {
                            step = 4;
                        } else {
                            step = 2;
                        }
                    }

                    break;
                case 4:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de cambio de matriz");
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "La matriz ha cambiado un " + aux[1]);
                        tI = I;
                        step = 2;
                    } else {
                        block();
                    }

                    break;
                case 5:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de EndGame");

                        for (int i = 0; i < oVStateActions.size(); i++) {
                            System.out.print(((StateAction) oVStateActions.get(i)).sState + "[");
                            for (int j = 0; j < ((StateAction) oVStateActions.get(i)).dValAction.length; j++) {
                                System.out.print(((StateAction) oVStateActions.get(i)).dValAction[j] + ",");
                            }
                            System.out.println("]");
                        }

                        G--;

                        if (G == 0) {
                            step = 0;
                        } else {
                            step++;
                        }
                    } else {
                        block();
                    }
                    break;
                case 6:
                    tR = R;
                    tI = I;
                    step = 1;
                    break;
            }
        }

    }

    private static void printPayoff(int[][] dPayoffAction) {
        for (int i = 0; i < dPayoffAction.length; i++) {
            System.out.print("buffer" + i + "[");
            for (int j = 0; j < dPayoffAction[i].length; j++) {
                System.out.print(dPayoffAction[i][j]);
                System.out.print(" ");
            }
            System.out.println("]");
        }
    }
}
