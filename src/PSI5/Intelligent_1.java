/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author expploitt
 */
public class Intelligent_1 extends Agent {

    private int id, R, I, S, N, P;
    private int position = 0;
    private int WINDOW;
    private int oponent;

    private final int FIXED = 10;
    private final int RANDOM = 11;
    private final int INTELLIGENT = 12;

    protected void setup() {

        System.out.println("Hola, soy un Intelligent Agent de tipo 1 y me llamo " + getAID().getName());

        //Register in the yellow pages as a player
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Player");
        sd.setName("Game");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }

        addBehaviour(new GamePerformer());
    }

    @Override
    public void takeDown() {
        System.out.println("Intelligent Agent de tipo 1 terminando...");
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class GamePerformer extends CyclicBehaviour {

        private int step = 0;
        private boolean player1 = false;

        private ACLMessage msg, reply;
        private MessageTemplate mt;

        private int tR = R, tI = I;
        private int[] windowsValues;
        private int windowsCount;

        private Componente[][] matrix;

        private FixedEstrategy fixedEstrategy;

        @Override
        public void action() {

            String[] aux;
            Componente componenteActual;

            switch (step) {
                case 0: //get ID of player
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo ID");
                        aux = msg.getContent().split("#");
                        id = Integer.parseInt(aux[1]);
                        aux = aux[2].split(",");

                        N = Integer.parseInt(aux[0]);
                        S = Integer.parseInt(aux[1]);
                        tR = R = Integer.parseInt(aux[2]);
                        tI = I = Integer.parseInt(aux[3]);
                        P = Integer.parseInt(aux[4]);
                        matrix = new Componente[S][S];
                        WINDOW = (int) Math.floor(R / 3);
                        windowsValues = new int[WINDOW];
                        System.out.println(" Window =" + WINDOW);
                        step++;
                    } else {
                        block();
                    }

                    break;
                case 1:  //get message of NEW GAME
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "Recibido mensaje " + aux[0]);
                        aux = aux[1].split(",");
                        System.out.println(getAID() + "Vamos a jugar jugador " + aux[0] + " y jugador " + aux[1]);

                        if (id == Integer.parseInt(aux[0])) {
                            player1 = true;
                            System.out.println("Is player A");
                        }

                        oponent = 0;
                        windowsCount = -1;
                        step++;
                    } else {
                        block();
                    }

                    break;
                case 2:  //REQUEST for choose position 
                    mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo REQUEST de Position");
                        reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setConversationId("Position");

                        if (oponent == FIXED && windowsCount > WINDOW) {
                            position = fixedEstrategy.maxPosition + 1;
                            reply.setContent("Position#" + (position - 1));
                            System.out.println("Position = " + (position - 1));
                        } else {
                            position = (new Random((long) S + windowsCount)).nextInt(S);
                            reply.setContent("Position#" + (position));
                            System.out.println("Position = " + (position));
                            windowsCount++;
                        }

                        send(reply);
                        tR--;
                        tI--;
                        step++;
                    } else {
                        block();
                    }
                    break;
                case 3:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo PUNTOS");

                        /*Ventana Temporal*/
                        if (windowsCount < windowsValues.length) {
                            if (player1) {
                                windowsValues[windowsCount] = Integer.parseInt(msg.getContent().split("#")[1].split(",")[1]);
                                System.out.println("Valor recibido soy player A = " + windowsValues[windowsCount]);
                            } else {
                                windowsValues[windowsCount] = Integer.parseInt(msg.getContent().split("#")[1].split(",")[0]);
                                System.out.println("Valor recibido NO soy player A = " + windowsValues[windowsCount]);
                            }
                            //windowsCount++;
                            System.out.println(windowsCount);
                        }

                        /*Comprobamos los resultados que hay en el array para saber si es un jugador
                           estático, aleatorio o inteligente*/
                        if (windowsCount == windowsValues.length) {
                            for (int a  : windowsValues) {
                                System.out.println("WV: " + a);
                            }
                            int lastValue = windowsValues[0];
                            for (int value : windowsValues) {
                                System.out.println("Value:" + value);
                                if (value != lastValue) {
                                    oponent = RANDOM;
                                    System.out.println("RANDOM");
                                    break;
                                } else {
                                    oponent = FIXED;
                                    System.out.println("FIXED STRATEGY");
                                    fixedEstrategy = new FixedEstrategy(matrix[lastValue]);
                                }
                            }
                            windowsCount++;
                        }

                        if (oponent == FIXED && player1) {
                            System.out.println("Posible eleccion player A " + Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]) + "," + Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]) + " "
                                    + Integer.parseInt(msg.getContent().split("#")[1].split(",")[0]));
                            fixedEstrategy.setPossibleElection(Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]), Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]),
                                    Integer.parseInt(msg.getContent().split("#")[1].split(",")[0]));
                        } else if (oponent == FIXED && !player1) {
                            System.out.println("Posible eleccion player A " + Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]) + "," + Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]) + " "
                                    + Integer.parseInt(msg.getContent().split("#")[1].split(",")[1]));
                            fixedEstrategy.setPossibleElection(Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]), Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]),
                                    Integer.parseInt(msg.getContent().split("#")[1].split(",")[1]));

                        }

                        if (tR == 0) {
                            step = 5;
                        } else if (tI == 0) {
                            step = 4;
                        } else {
                            step = 2;
                        }
                        //x++;
                    }

                    break;
                case 4:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de cambio de matriz");
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "La matriz ha cambiado un " + aux[1]);
                        tI = I;
                        step = 2;
                    } else {
                        block();
                    }

                    break;
                case 5:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de EndGame");
                        step++;
                    } else {
                        block();
                    }
                    break;
                case 6:
                    tR = R;
                    tI = I;
                    step = 1;
                    break;
            }
        }
    }

    private class FixedEstrategy {

        private Componente[] elections;
        private int maxPosition;

        /**
         *
         * @param size
         */
        public FixedEstrategy(Componente[] array) {
            elections = array;
            for (Componente election : elections) {
                election = null;
            }
            maxPosition = 0;
        }

        /**
         * Método para setear un conjunto de valores posibles.
         *
         * @param value1 Siempre serán los puntos que recibiremos si elegimos
         * esta opcion
         * @param value2 Siempre serán los puntos que recibirá el contricante si
         * elegimos esta opción
         * @param col
         */
        public void setPossibleElection(int value1, int value2, int col) {
            elections[col] = new Componente(value1, value2);
            calculateMinMaxPosition();
        }

        /**
         *
         */
        private void calculateMinMaxPosition() {
            for (int i = 0; i < elections.length; i++) {
                if (elections[i] != null) {
                    System.out.println("election max/i : " + elections[maxPosition].getI() + "election i/i" + elections[i].getI() + "election i/j" + elections[i].getJ());
                    if (elections[maxPosition].getI() < elections[i].getI() && elections[i].getI() < elections[i].getJ()) {
                        maxPosition = i;
                        System.out.println("MAX POSITION " + maxPosition);
                    }
                } else if (elections[i] == null) {
                    maxPosition = i;
                    System.out.println("MAX POSITION porque hay null" + maxPosition);
                    return;
                }
            }
        }

    }
}
