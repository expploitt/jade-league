/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

/**
 *
 * @author expploitt
 */
public class Componente {

    private int i;
    private int j;

    public Componente(int i, int j) {
        this.setI(i);
        this.setJ(j);
    }
     
    public Componente() {
        setI(0);
        setJ(0);
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }

    /**
     * @return the j
     */
    public int getJ() {
        return j;
    }

    /**
     * @param j the j to set
     */
    public void setJ(int j) {
        this.j = j;
    }
    
    @Override
    public String toString(){
        return "(" + i + ", " + j + ")";
    }
}
