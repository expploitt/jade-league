/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author expploitt
 */
public class MainAgent extends Agent {

    private int N = 0;
    private int S = 0;
    private int R = 0;
    private int I = 0;
    private int G = 0;
    private int P = 0;

    private Componente[][] matriz = null;
    private Interfaz gui;
    private Parameters parametros;
    private AID[] playerAgents;
    int idActual = 0;

    @Override
    protected void setup() {

        System.out.println("Soy el Main Agent y me llamo " + getAID().getName());
        gui = new Interfaz(parametros, this);
        parametros = new Parameters(this, gui);
        gui.setParam(parametros);
        parametros.showGui();

    }

    @Override
    public void takeDown() {
        System.out.println("Soy el Main Agent y me llamo " + getAID().getName() + " y voy a finalizar mi trabajo");
    }

    public void saveParameters(final int players, final int size, final int rounds, final int roundsToChange, final int percentage) {
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                N = players;
                R = rounds;
                I = roundsToChange;
                S = size;
                P = percentage;
                G = (N * (N - 1)) / 2;
                System.out.println("Guardando parámetros!");
            }
        });
    }

    public void updatePlayers() {

        gui.logLine("Buscando jugadores");
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Player");
        template.addServices(sd);

        try {
            DFAgentDescription[] result = DFService.search(this, template);
            gui.logLine("Found " + result.length + " players");
            playerAgents = new AID[result.length];
            for (int i = 0; i < result.length; ++i) {
                playerAgents[i] = result[i].getName();
                System.out.println(result[i].getName());
            }

        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Provisional
        String[] playerNames = new String[playerAgents.length];
        for (int i = 0; i < playerAgents.length; i++) {
            playerNames[i] = playerAgents[i].getName();
            System.out.println(playerNames[i]);
        }

        gui.cleanLogPlayers();
        gui.logPlayers(playerNames);

        if (playerNames.length < N) {
            gui.notPossibleGame(N);
        } else {
            for (int i = 0; i < N; i++) {
                playerNames[i] = playerAgents[i].getName();
            }
            gui.enableStartLeague();
            gui.logLine("Van a jugar " + playerNames.length + " jugadores");
        }

    }

    public void createMatriz(String player1, String player2) {
        gui.setInLogMatrix("Generando la matriz de tamaño " + S + "x" + S + "   " + player1 + " vs " + player2);
        matriz = new Componente[S][S];

        for (int i = 0; i < S; i++) {
            for (int j = 0; j < S; j++) {
                if (i == j) { //si es en la diagonal
                    int x = (int) (Math.random() * 9);
                    matriz[i][j] = new Componente(x, x);
                }
                if (j > i) {
                    matriz[i][j] = new Componente((int) (Math.random() * 9), (int) (Math.random() * 9));
                    matriz[j][i] = matriz[i][j];
                }
            }
        }

        paintMatriz();
    }

    public void changeMatriz() {
        /*addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {*/
        Random rand;
        int upperComponents, downComponents, percentagePerComponent, downError, upperError, components;
        gui.setInLogMatrix("Cambiando la matriz un " + P + "%");

        rand = new Random(65464151);
        components = S * S;

        System.out.println("Component" + components);
        System.out.println("percentage" + P);
        upperComponents = (int) Math.ceil((float) components * P / 100);
        downComponents = (int) Math.floor((float) components * P / 100);
        percentagePerComponent = (int) Math.round(100 / components);
        System.out.println("percentagePerComponent" + percentagePerComponent);
        System.out.println("upperComponents " + upperComponents + "downComponent " + downComponents);
        downError = (int) (P) - (percentagePerComponent * downComponents);
        upperError = (percentagePerComponent * upperComponents) - (int) (P);
        System.out.println("down: " + downError + " upper: " + upperError);

        /*Comprobamos cual es el menor e*/
        int componentsToChange;

        if (downError > upperError) {
            componentsToChange = upperComponents;
        } else if (upperError > downComponents) {
            componentsToChange = downComponents;
        } else { //los errores son iguales y da igual asignar una que otra
            componentsToChange = downComponents;
        }

        System.out.println("ComponentToChange: " + componentsToChange);
        System.out.println("------------------");
        while (componentsToChange > 0) {
            int i = (int) rand.nextInt(S);
            int j = (int) rand.nextInt(S);
            System.out.println("i: " + i + " j: " + j);

            if (i != j) {
                if ((componentsToChange - 2) < 0) {
                    System.out.println("continue");
                    System.out.println("componentsTochange:" + componentsToChange);
                    continue;
                }
                System.out.println("Distinct");
                matriz[i][j] = new Componente((int) rand.nextInt(9), (int) rand.nextInt(9));
                matriz[j][i] = matriz[i][j];
                System.out.println(matriz[i][j]);
                componentsToChange -= 2;
            } else {
                System.out.println("equals");
                int x = (int) rand.nextInt(9);
                matriz[i][j] = new Componente(x, x);
                System.out.println(matriz[i][j]);
                componentsToChange -= 1;
            }
            System.out.println("componentsTochange:" + componentsToChange);
        }

        paintMatriz();
    }

    /* }
        );
    }*/
    public void paintMatriz() {

        String string = "";

        for (Componente[] matriz1 : matriz) {
            for (int j = 0; j < matriz.length; j++) {
                string = string + matriz1[j].toString() + "  ";
            }
            string = string + "\n";
        }

        gui.setInLogMatrix("Nueva matriz \n" + string);
        gui.logLine("Nueva matriz\n" + string);
        System.out.println(string);
    }

    public void startLeague() {
        gui.logLine("Comenzando liga");
        addBehaviour(new GameManager());
    }

    private class GameManager extends SimpleBehaviour {

        private ArrayList<Player> players = new ArrayList<>();

        @Override
        public void action() {
            //Assign the IDs

            int lastId = 0;

            for (AID a : playerAgents) {
                players.add(new Player(a, lastId++));
            }

            //Initialize (inform ID)
            for (Player player : players) {
                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                gui.logLine("Main enviando -> " + "Id#" + player.getId() + "#" + N + "," + S + "," + R + "," + I + "," + P + " a " + player.getAid());
                msg.setContent("Id#" + player.getId() + "#" + N + "," + S + "," + R + "," + I + "," + P);
                System.out.println(player.getAid());
                msg.addReceiver(player.getAid());
                send(msg);
            }
            //Organize the matches
            for (int i = 0; i < players.size(); i++) {
                for (int j = i + 1; j < players.size(); j++) { //too lazy to think, let's see if it works or it breaks
                    gui.logLine("-----------------------------------------------------");
                    gui.logLine("Nueva partida entre los jugadores " + players.get(i).getId() + " y "
                            + players.get(j).getId());
                    playGame(players.get(i), players.get(j));
                }
            }

            System.out.println("Results");
            ArrayList<Player> tiePlayers = new ArrayList<>();
            Player winner = new Player();
            winner.setPoints(-1); //force to get always a real player with more points than these.

            for (Player player : players) {
                if (player.getPoints() > winner.getPoints()) {
                    winner = player;
                } else if (player.getPoints() == winner.getPoints()) {
                    tiePlayers.add(player);
                }
                System.out.println("Puntos totales para jugador " + player.getAid() + ":" + player.getPoints());
            }

            if (tiePlayers.size() > 1) { //There are tie players
                gui.theWinnersAre(tiePlayers);
            } else {
                System.out.println("The winner is:" + winner.getAid());
                gui.theWinnerIs(winner);
                gui.logLine("El ganador es: " + winner.getAid().getName() + " con " + winner.getPoints());
            }

        }

        @Override
        public boolean done() {
            return true;
        }

        public void playGame(Player player1, Player player2) {

            int step = 0;
            int tR = R, tI = I;
            int row = 0, col = 0;

            do {
                switch (step) {
                    case 0:
                        //Assuming player1.id < player2.id
                        createMatriz(player1.getAid().getLocalName(), player2.getAid().getLocalName());
                        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                        msg.addReceiver(player1.getAid());
                        msg.addReceiver(player2.getAid());
                        gui.logLine("Main enviando -> " + "NewGame#" + player1.getId() + "," + player2.getId());
                        msg.setContent("NewGame#" + player1.getId() + "," + player2.getId());
                        send(msg);
                        step++;
                        break;
                    case 1:
                        msg = new ACLMessage(ACLMessage.REQUEST);
                        msg.setContent("Position");
                        msg.addReceiver(player1.getAid());
                        send(msg);
                        step++;
                        break;
                    case 2:
                        ACLMessage move1 = blockingReceive();
                        gui.logLine("Main recibe -> " + (Integer.parseInt(move1.getContent().split("#")[1]) + 1) + " de " + move1.getSender().getName());
                        row = Integer.parseInt(move1.getContent().split("#")[1]);
                        step++;
                        break;
                    case 3:
                        msg = new ACLMessage(ACLMessage.REQUEST);
                        msg.setContent("Position");
                        msg.addReceiver(player2.getAid());
                        send(msg);
                        step++;
                        break;
                    case 4:
                        ACLMessage move2 = blockingReceive();
                        gui.logLine("Main recibe -> " + (Integer.parseInt(move2.getContent().split("#")[1]) + 1) + " de " + move2.getSender().getName());
                        col = Integer.parseInt(move2.getContent().split("#")[1]);
                        step++;
                        break;
                    case 5:
                        player1.setPoints(player1.getPoints() + matriz[row][col].getI());
                        player2.setPoints(player2.getPoints() + matriz[row][col].getJ());
                        gui.logLine("Puntos totales de " + player1.getAid().getName() + ": " + player1.getPoints() + " puntos");
                        gui.logLine("Puntos totales de " + player2.getAid().getName() + ": " + player2.getPoints() + " puntos");
                        System.out.println("Puntos totales de playerA " + player1.getPoints());
                        System.out.println("Puntos totales de playerB " + player2.getPoints());
                        gui.updateTable(players);
                        step++;
                        break;
                    case 6:
                        msg = new ACLMessage(ACLMessage.INFORM);
                        msg.addReceiver(player1.getAid());
                        msg.addReceiver(player2.getAid());
                        msg.setContent("Results#" + row + "," + col + "#" + matriz[row][col].getI() + "," + matriz[row][col].getJ());
                        send(msg);
                        step++;
                        break;
                    case 7:
                        tR--;
                        tI--;

                        if (tR == 0) {
                            step = 9;
                        } else if (tI == 0) {
                            changeMatriz();
                            tI = I;
                            step = 8;
                        } else {
                            step = 1;
                        }
                        break;
                    case 8:
                        msg = new ACLMessage(ACLMessage.INFORM);
                        msg.addReceiver(player1.getAid());
                        msg.addReceiver(player2.getAid());
                        msg.setContent("Change#" + P);
                        send(msg);
                        step = 1;
                        break;
                    case 9:
                        msg = new ACLMessage(ACLMessage.INFORM);
                        msg.addReceiver(player1.getAid());
                        msg.addReceiver(player2.getAid());
                        msg.setContent("EndGame");
                        send(msg);
                        step++;
                        break;
                }
            } while (step != 10);
        }
    }
}
