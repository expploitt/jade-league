/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author expploitt
 */
public class FixedPlayer extends Agent {

    private int id, R, I, S, N, P, G;
    private int position = 0;

    protected void setup() {

        System.out.println("Hola, soy un Fixed Player y me llamo " + getAID().getName());

        //Register in the yellow pages as a player
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Player");
        sd.setName("Game");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }

        addBehaviour(new GamePerformer());

    }

    @Override
    public void takeDown() {
        System.out.println("Fixed Player terminando...");
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class GamePerformer extends CyclicBehaviour {

        private int step = 0;
        private ACLMessage msg, reply;
        private MessageTemplate mt;
        private int tR = R, tI = I;
        private Random rand;

        @Override
        public void action() {
            String[] aux;
            switch (step) {
                case 0: //get ID of player
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo ID");
                        aux = msg.getContent().split("#");
                        id = Integer.parseInt(aux[1]);
                        aux = aux[2].split(",");

                        N = Integer.parseInt(aux[0]);
                        S = Integer.parseInt(aux[1]);
                        tR = R = Integer.parseInt(aux[2]);
                        tI = I = Integer.parseInt(aux[3]);
                        P = Integer.parseInt(aux[4]);
                        G = N - 1;

                        rand = new Random(id + 1000);
                        position = rand.nextInt(S);
                        position = rand.nextInt(S);
                        step++;
                    } else {
                        block();
                    }

                    break;
                case 1:  //get message of NEW GAME
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        aux = msg.getContent().split("#");
                        //System.out.println(getAID() + "Recibido mensaje " + aux[0]);
                        aux = aux[1].split(",");
                        //System.out.println(getAID() + "Vamos a jugar jugador " + aux[0] + " y jugador " + aux[1]);
                        step++;
                    } else {
                        block();
                    }

                    break;
                case 2:  //REQUEST for choose position 
                    mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo REQUEST de Position");
                        reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setConversationId("Position");
                        reply.setContent("Position#" + position);
                        System.out.println("Position = " + position);

                        send(reply);
                        tR--;
                        tI--;
                        step++;
                    } else {
                        block();
                    }
                    break;
                case 3:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo PUNTOS");
                        /*aux = msg.getContent().split("#");

                        if (playerA) { //es el jugador A
                            System.out.println(getAID() + "Recibidos " + aux[2].split(",")[0]);

                        } else { //es el jugador B
                            System.out.println(getAID() + "Recibidos " + aux[2].split(",")[1]);
                        }*/

                        if (tR == 0) {
                            step = 5;
                        } else if (tI == 0) {
                            step = 4;
                        } else {
                            step = 2;
                        }
                    }

                    break;
                case 4:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de cambio de matriz");
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "La matriz ha cambiado un " + aux[1]);
                        tI = I;
                        step = 2;
                    } else {
                        block();
                    }

                    break;
                case 5:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de EndGame");

                        G--;

                        if (G == 0) {
                            step = 0;
                        } else {
                            step++;
                        }
                    } else {
                        block();
                    }

                    break;
                case 6:
                    tR = R;
                    tI = I;
                    step = 1;
                    break;
            }
        }
    }
}
