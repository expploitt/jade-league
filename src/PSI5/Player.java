/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.AID;

/**
 *
 * @author expploitt
 */
public class Player {

    private AID aid;
    private int id, points;

    public Player(){
        this.points = 0;
    }
    
    public Player(AID aid, int id) {
        this.aid = aid;
        this.id = id;
        this.points = 0;
    }

    /**
     * @return the aid
     */
    public AID getAid() {
        return aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(AID aid) {
        this.aid = aid;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }
}
