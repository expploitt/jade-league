/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PSI5;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.Serializable;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author expploitt
 */
public class Intelligent_2 extends Agent {

    final double dDecFactorLR = 0.99;			// Value that will decrement the learning rate in each generation
    final double dEpsilon = 0.2;				// Used to avoid selecting always the best action
    final double dMINLearnRate = 0.05;			// We keep learning, after convergence, during 5% of times
    final double dGamma = 0.6;

    double dLastFunEval;
    double dLearnRate = 0.9;

    int iNumActions = 0; 					// For C or D for instance
    int iLastAction;					// The last action that has been played by this player
    int iAction;
    int iNewAction;

    int[] iNumTimesAction = new int[iNumActions];		// Number of times an action has been played
    double[] dPayoffAction = new double[iNumActions];	// Accumulated payoff obtained by the different actions

    StateAction oPresentStateAction;			// Contains the present state we are and the actions that are available
    Vector oVStateActions;					// A vector containing strings with the possible States and Actions available at each one
    StateAction oLastStateAction;

    private int id, R, I, S, N, P, G;
    private int position = 0;

    protected void setup() {

        System.out.println("Hola, soy un Intelligent Agent de tipo 1 y me llamo " + getAID().getName());

        //Register in the yellow pages as a player
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Player");
        sd.setName("Game");
        dfd.addServices(sd);

        oVStateActions = new Vector();

        try {
            DFService.register(this, dfd);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }

        addBehaviour(new Intelligent_2.GamePerformer());
    }

    @Override
    public void takeDown() {
        System.out.println("Intelligent Agent de tipo 2 terminando...");
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            Logger.getLogger(FixedPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is used to implement Q-Learning: 1. I start with the last
     * action a, the previous state s and find the actual state s' 2. Select the
     * new action with Qmax{a'} 3. Adjust: Q(s,a) = Q(s,a) + dLearnRateLR [R +
     * dGamma . Qmax{a'}(s',a') - Q(s,a)] 4. Select the new action by a
     * epsilon-greedy methodology
     *
     * @param sState contains the present state
     * @param iNActions contains the number of actions that can be applied in
     * this state
     * @param dFunEval is the new value of the function to evaluate (depends on
     * the game). This value minus last value also determines the reward to be
     * used.
     */
    public void vGetNewActionQLearning(String sState, int iNActions, double dFunEval) {
        boolean bFound;
        int iBest = -1, iNumBest = 1;
        double dR, dQmax;
        StateAction oStateAction;

        bFound = false;							// Searching if we already have the state
        for (int i = 0; i < oVStateActions.size(); i++) {
            oStateAction = (StateAction) oVStateActions.elementAt(i);
            System.out.println("Nombre encontrado - " + oStateAction.sState + "nombre a encontrar - " + sState);
            if (oStateAction.sState.equals(sState)) {
                oPresentStateAction = oStateAction;
                bFound = true;
                break;
            }
        }
        // If we didn't find it, then we add it
        if (!bFound) {
            System.out.println("-------------------No encontrado - " + sState);
            oPresentStateAction = new StateAction(sState, iNActions);
            oVStateActions.add(oPresentStateAction);
        }

        dQmax = 0;
        for (int i = 0; i < iNActions; i++) {					// Determining the action to get Qmax{a'}
            if (oPresentStateAction.dValAction[i] > dQmax) {
                iBest = i;
                iNumBest = 1;							// Reseting the number of best actions
                dQmax = oPresentStateAction.dValAction[i];
            } else if ((oPresentStateAction.dValAction[i] == dQmax) && (dQmax > 0)) {	// If there is another one equal we must select one of them randomly
                iNumBest++;
                if (Math.random() < 1.0 / (double) iNumBest) {				// Choose randomly with reducing probabilities
                    iBest = i;
                    dQmax = oPresentStateAction.dValAction[i];
                }
            }
        }
        // Adjusting Q(s,a)
        if (oLastStateAction != null) {
            dR = dFunEval - dLastFunEval;					// Note that dR is also used as reward in the QL formulae
            if (dFunEval >= 0) // If reward grows and the previous action was allowed --> reinforce the previous action considering present values
            {
                System.out.println("dValAction = " + oLastStateAction.dValAction[iAction]);
                oLastStateAction.dValAction[iAction] += dLearnRate * (dFunEval + dGamma * dQmax - oLastStateAction.dValAction[iAction]);
                System.out.println("dValAction = " + oLastStateAction.dValAction[iAction]);
            }
        }

        if ((iBest > -1) && (Math.random() > dEpsilon)) // Using the e-greedy policy to select the best action or any of the rest
        {
            iNewAction = iBest;
            System.out.println("Best answer " + iNewAction);
        } else {
            do {
                iNewAction = (int) (Math.random() * (double) iNumActions);
                System.out.println("Random Answer " + iNewAction);
            } while (iNewAction == iBest);
        }

        oLastStateAction = oPresentStateAction;				// Updating values for the next time
        dLastFunEval = dFunEval;
        dLearnRate *= dDecFactorLR;						// Reducing the learning rate
        if (dLearnRate < dMINLearnRate) {
            dLearnRate = dMINLearnRate;
        }
    }

    /**
     * This is the basic class to store Q values (or probabilities) and actions
     * for a certain state
     *
     * @author Juan C. Burguillo Rial
     * @version 2.0
     */
    private class StateAction implements Serializable {

        String sState;
        double[] dValAction;

        StateAction(String sAuxState, int iNActions) {
            sState = sAuxState;
            dValAction = new double[iNActions];
        }

        public String sGetState() {
            return sState;
        }

        public double dGetQAction(int i) {
            return dValAction[i];
        }
    }

    private class GamePerformer extends CyclicBehaviour {

        private int step = 0;
        private ACLMessage msg, reply;
        private MessageTemplate mt;
        private int tR = R, tI = I;
        private Random rand = new Random(id + 1000);
        boolean player1;

        @Override
        public void action() {
            String[] aux;
            switch (step) {
                case 0: //get ID of player
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo ID");
                        aux = msg.getContent().split("#");
                        id = Integer.parseInt(aux[1]);
                        System.out.println(getAID() + " " + id);
                        aux = aux[2].split(",");

                        N = Integer.parseInt(aux[0]);
                        S = Integer.parseInt(aux[1]);
                        tR = R = Integer.parseInt(aux[2]);
                        tI = I = Integer.parseInt(aux[3]);
                        P = Integer.parseInt(aux[4]);
                        G = N - 1;

                        

                        step++;
                    } else {
                        block();
                    }

                    break;
                case 1:  //get message of NEW GAME
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "Recibido mensaje " + aux[0]);
                        aux = aux[1].split(",");
                        System.out.println(getAID() + "Vamos a jugar jugador " + aux[0] + " y jugador " + aux[1]);

                        if (id == Integer.parseInt(aux[0])) {
                            player1 = true;
                            System.out.println("Is player A");
                        }
               
                        for (int i = 0; i < S; i++) {  //Inicializamos la matriz de estados
                            oVStateActions.add(new StateAction("state" + i, S));
                        }

                        oPresentStateAction = (StateAction) oVStateActions.get(0);
                        oLastStateAction = (StateAction) oVStateActions.get(0);
                        iNumActions = S;
                        dLearnRate = 0.9;
                        step++;
                        
                    } else {
                        block();
                    }

                    break;
                case 2:  //REQUEST for choose position 
                    mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo REQUEST de Position");
                        reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setConversationId("Position");

                        position = iNewAction;
                        reply.setContent("Position#" + position);
                        System.out.println("Position = " + position);

                        send(reply);
                        tR--;
                        tI--;
                        step++;
                    } else {
                        block();
                    }
                    break;
                case 3:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo PUNTOS");

                        int difPoints;

                        if (player1) {
                            difPoints = Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]) - Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]);
                        } else {
                            difPoints = Integer.parseInt(msg.getContent().split("#")[2].split(",")[1]) - Integer.parseInt(msg.getContent().split("#")[2].split(",")[0]);
                        }

                        if (difPoints <= 0) {
                            difPoints = -1;
                        } else {
                            difPoints = 5;
                        }

                        System.out.println("DifPoint: " + difPoints);

                        vGetNewActionQLearning(oPresentStateAction.sGetState(), S, difPoints);
                        oPresentStateAction = (StateAction) oVStateActions.get(iNewAction);
                        iAction = iNewAction;

                        if (tR == 0) {
                            step = 5;
                        } else if (tI == 0) {
                            step = 4;
                        } else {
                            step = 2;
                        }
                    }

                    break;
                case 4:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de cambio de matriz");
                        aux = msg.getContent().split("#");
                        System.out.println(getAID() + "La matriz ha cambiado un " + aux[1]);
                        tI = I;
                        step = 2;
                    } else {
                        block();
                    }

                    break;
                case 5:
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = receive(mt);

                    if (msg != null) {
                        System.out.println(getAID() + "Recibiendo mensaje de EndGame");

                        for (int i = 0; i < oVStateActions.size(); i++) {
                            System.out.print(((StateAction) oVStateActions.get(i)).sState + "[");
                            for (int j = 0; j < ((StateAction) oVStateActions.get(i)).dValAction.length; j++) {
                                System.out.print(((StateAction) oVStateActions.get(i)).dValAction[j] + ",");
                            }
                            System.out.println("]");
                        }
                        
                        G--;

                        if (G == 0) {
                            step = 0;
                        } else {
                            step++;
                        }
                    } else {
                        block();
                    }
                    break;
                case 6:
                    tR = R;
                    tI = I;
                    step = 1;
                    break;
            }
        }
    }
}
